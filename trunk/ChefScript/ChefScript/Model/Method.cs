﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ChefScript.Model
{
    abstract class Method
    {
        public string Name { get; private set; }
        public Type TargetType { get; private set; }
        public List<Argument> Arguments = new List<Argument>();

        public MethodSignature Signature { get { return new MethodSignature(Name, TargetType, Arguments); } }

        private Method() { }

        public Method(string name, Type targetType)
        {
            Name = name;
            TargetType = targetType;
        }

        public abstract List<Variable> Execute(List<Variable> arguments, List<Variable> local); // Local variables for lambdas
    }
}
