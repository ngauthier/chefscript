﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ChefScript.Model
{
    class Variable<T> : Variable
    {
        public T Value { get; set; }

        public Variable(Type type, string name, bool isCollection)
            : base(type, name, isCollection) { }
    }

    class Variable
    {
        public Type Type { get; private set; }
        public string Name { get; private set; }
        public bool IsCollection { get; private set; }

        private Variable() { }

        public Variable(Type type, string name, bool isCollection)
        {
            Type = type;
            Name = name;
            IsCollection = isCollection;
        }
    }
}
