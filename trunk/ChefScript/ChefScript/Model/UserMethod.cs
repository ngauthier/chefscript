﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ChefScript.Model
{
    class UserMethod : Method
    {
        public readonly List<Statement> Statements = new List<Statement>();

        public UserMethod(string name, Type targetType) : base(name, targetType) { }

        // Local variables for lambdas
        public override List<Variable> Execute(List<Variable> arguments, List<Variable> local)
        {
            return new List<Variable>(); // TODO
        }
    }
}
