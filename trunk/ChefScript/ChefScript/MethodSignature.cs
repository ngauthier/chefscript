﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ChefScript.Model;

namespace ChefScript
{
    class MethodSignature : Tuple<string, ChefScript.Model.Type, List<Argument>>
    {
        public string Name { get { return Item1; } }
        public ChefScript.Model.Type TargetType { get { return Item2; } }
        public List<Argument> arguments { get { return Item3; } }

        public MethodSignature(String name, ChefScript.Model.Type targetType, List<Argument> arguments)
            : base(name, targetType, arguments)
        {
        }
    }
}
