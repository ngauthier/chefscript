﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ChefScript.Model
{
    class Argument
    {
        public Type Type { get; private set; }
        public string Name { get; private set; }
        public bool IsCollection { get; private set; }

        private Argument() { }

        public Argument(Type type, string name, bool isCollection)
        {
            Type = type;
            Name = name;
            IsCollection = isCollection;
        }
    }
}
