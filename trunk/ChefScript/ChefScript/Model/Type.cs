﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ChefScript.Model
{

    abstract class Type
    {
        public virtual string Name { get; private set; }
        public Type Parent { get; private set; }

        public readonly List<Variable> Variables = new List<Variable>();

        private Type() { }

        public Type(string name, Type parent = null)
        {
            Name = name;
            Parent = parent;
        }
    }
}
