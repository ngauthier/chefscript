﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ChefScript.Model
{
    class NativeType : Type
    {
        private static List<Type> _nativeTypes = new List<Type>();

        private NativeType(string name) : base(name) 
        { 
            _nativeTypes.Add(this);
        }

        public static IEnumerable<Type> All { get { return _nativeTypes.ToArray(); } }

        public static NativeType Integer = new NativeType("antye");
        public static NativeType Float = new NativeType("senp");
        public static NativeType Double = new NativeType("doub");
        public static NativeType Char = new NativeType("let");
        public static NativeType String = new NativeType("teks");
        public static NativeType Start = new NativeType("depa");
    }
}
