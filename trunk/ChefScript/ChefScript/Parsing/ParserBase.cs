﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace ChefScript.Parsing
{
    class ParserBase
    {
        public Token GetNextToken(Stream stream, ref int lineCount)
        {
            var first = GetNextRelevantChar(stream, ref lineCount);

            if (!first.HasValue)
                return Token.EndOfFile;

            if (first == Token.OpeningBrace.Value[0])
                return Token.OpeningBrace;
            if (first == Token.ClosingBrace.Value[0])
                return Token.ClosingBrace;
            if (first == Token.OpeningParentheses.Value[0])
                return Token.OpeningParentheses;
            if (first == Token.ClosingParentheses.Value[0])
                return Token.ClosingParentheses;
            if (first == Token.Colon.Value[0])
                return Token.Colon;
            if (first == Token.SemiColon.Value[0])
                return Token.SemiColon;

            var builder = new StringBuilder();
            builder.Append(first);

            if (char.IsDigit(first.Value))
            {
                char? c;
                while (true)
                {
                    c = GetNextChar(stream);
                    if (char.IsLetter(c.Value))
                    {
                        return Token.Invalid;
                    }
                    else if (char.IsDigit(c.Value))
                    {
                        builder.Append(c);
                        continue;
                    }
                    Backtrack(stream);
                    break;
                }

                return new DigitToken(builder.ToString());
            }

            if (char.IsLetter(first.Value))
            {
                char? c;
                while (true)
                {
                    c = GetNextChar(stream);
                    if (char.IsLetterOrDigit(c.Value))
                    {
                        builder.Append(c);
                        continue;
                    }
                    Backtrack(stream);
                    break;
                }

                return new Token(builder.ToString());
            }
            
            return Token.Invalid;
        }

        private char? GetNextRelevantChar(Stream stream, ref int lineCount)
        {
            char? c;
            while (true)
            {
                c = GetNextChar(stream);
                if (!c.HasValue)
                    return c;

                switch (c.Value)
                {
                    case ' ':
                        continue;
                    case '\n':
                    case '\r':
                        lineCount++;
                        continue;
                    default:
                        return c;
                }
            }
        }

        private char? GetNextChar(Stream stream)
        {
            var c = stream.ReadByte();

            if (c == -1)
                return null;

            return (char)c;
        }

        private char Backtrack(Stream stream)
        {
            return (char)stream.Seek(-1, SeekOrigin.Current);
        }
    }
}
