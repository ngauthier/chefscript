﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ChefScript.Model
{
    class NativeMethod : Method
    {
        private static List<NativeMethod> _nativeMethods = new List<NativeMethod>();

        private NativeMethod(string name, Type targetType) : base(name, targetType) 
        {
            _nativeMethods.Add(this);
        }

        // Local variables for lambdas
        public override List<Variable> Execute(List<Variable> arguments, List<Variable> local)
        {
            return new List<Variable>(); // TODO
        }

        public static IEnumerable<NativeMethod> All { get { return _nativeMethods.ToArray(); } }

        public static NativeMethod Integer = new NativeMethod("ekri", NativeType.Start);
    }
}
