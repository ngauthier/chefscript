﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ChefScript.Parsing
{
    class Token
    {
        public string Value { get; private set; }

        public Token(string value)
        {
            Value = value;
        }

        public static Token EndOfFile = new Token(string.Empty);
        public static Token Invalid = new Token(string.Empty);

        public static Token OpeningBrace = new Token("{");
        public static Token ClosingBrace = new Token("}");
        public static Token OpeningParentheses = new Token("(");
        public static Token ClosingParentheses = new Token(")");
        public static Token Colon = new Token(":");
        public static Token SemiColon = new Token(";");
        public static Token FunctionDefinition = new Token("CHEF");
        public static Token MethodDefinition = new Token("OBJE");
        public static Token InheritanceOperator = new Token("SE");
        public static Token EnumerationPrefix = new Token("anpil");
        public static Token ImplicitThis = new Token("sa");
    }

    class DigitToken : Token
    {
        public DigitToken(string value) : base(value) { }
    }
}
