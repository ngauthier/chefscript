﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ChefScript.Parsing;

namespace ChefScript
{
    class Program
    {
        static void Main(string[] args)
        {
            var interpreter = new Interpreter();
            var entryPoint = interpreter.EntryPoint;

            var parser = new Parser(interpreter);
            parser.ParseCurrentDirectory();

            Console.ReadKey();
        }
    }
}
