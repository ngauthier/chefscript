﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace ChefScript.Parsing
{
    class Parser : ParserBase
    {
        public Interpreter Interpreter { get; private set; }

        private Parser() { }
        public Parser(Interpreter interpreter)
        {
            Interpreter = interpreter;
        }

        public bool ParseCurrentDirectory()
        {
            return ParseDirectory(Directory.GetCurrentDirectory());
        }

        public bool ParseDirectory(string path)
        {
            foreach (var file in Directory.GetFiles(path, "*.CHEF", SearchOption.AllDirectories))
                if (!ParseFile(file))
                    return false;
            return true;
        }

        public bool ParseFile(string path)
        {
            var file = File.OpenRead(path);
            int lineCount = 1;
            Token token;
            while (true)
            {
                token = GetNextToken(file, ref lineCount);

                if (token == Token.EndOfFile)
                {
                    Console.WriteLine("EOF");
                    return true;
                }

                if (token == Token.Invalid)
                {
                    Console.WriteLine(string.Format("Invalid Token on line {0}.", lineCount));
                    return false;
                }

                Console.WriteLine(token.Value);
            }
            
        }
    }
}
