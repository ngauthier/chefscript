﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ChefScript.Model;

namespace ChefScript
{
    class Interpreter
    {
        private readonly MethodSignature _startSignature = new MethodSignature("depa", NativeType.Start, new List<Argument>());

        private Dictionary<string, ChefScript.Model.Type> _types;
        private Dictionary<MethodSignature, Method> _methods;

        public Interpreter()
        {
            _types = NativeType.All.ToDictionary(x => x.Name);
            _methods = NativeMethod.All.ToDictionary(x => x.Signature, x => (Method)x);
        }

        public Method EntryPoint 
        {
            get 
            {
                Method entryPoint;
                if (_methods.TryGetValue(_startSignature, out entryPoint))
                    return entryPoint;
                return null;
            } 
        }
    }
}
